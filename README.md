## NTDSex

[![Version](https://img.shields.io/badge/Version-1.0-green.svg)](#) [![PowerShell DSInternals](https://img.shields.io/badge/Dependance-DSInternals-007bb8.svg?logo=PowerShell)](#)

### Description

![NTDSex](./.ntdsex.gif "NTDSex")

This tool helps to extract hashes from the Active Directoty database (.dit).

It is possible to extract the whole database or to print a single SamAccountName.

> This tool requires [DSInternal](https://github.com/MichaelGrafnetter/DSInternals)

```powershell
Install-Module DSInternals -Force
```

### Parameters

| Parameter | Description                                               |
| :-------- | :-------------------------------------------------------- |
| NTDS      | Path to an Active Directoty database (`ntds.dit`).        |
| Hive      | Path to an offline `SYSTEM` registry hive.                |
| User      | An `SamAccountName` to get information about the account. |
| Output    | Output file to export the list of hashes.                 |

### Example

```powershell
D:\PS> .\NTDSex.ps1 -NTDS .\ntds.dit -Hive .\SYSTEM -Output .\ntds.txt
D:\PS> .\NTDSex.ps1 -NTDS .\ntds.dit -Hive .\SYSTEM -User krbtgt
```
