<#
.SYNOPSIS
Extract/Exports and Check Active Directory account(s).

.DESCRIPTION
This tool helps to extract hashes from the Active Directoty database (.dit).
It is possible to extract the whole database or to print a single SamAccountName.

.PARAMETER NTDS
Path to an Active Directoty database (.dit).

.PARAMETER Hive
Path to an offline SYSTEM registry hive.

.PARAMETER User
An SamAccountName to get information about the account.

.PARAMETER Output
Output file to export the list of hashes.

.EXAMPLES
D:\PS> .\NTDSex.ps1 -NTDS .\ntds.dit -Hive .\SYSTEM -Output .\ntds.txt
D:\PS> .\NTDSex.ps1 -NTDS .\ntds.dit -Hive .\SYSTEM -User krbtgt

.NOTES
Author: KASH
Filename: NTDSex.ps1
#>

[CmdletBinding()]
Param(
	[Parameter(Position=0,mandatory=$false)]
	[String]$NTDS,
	[Parameter(Position=1,mandatory=$false)]
	[String]$Hive,
	[Parameter(Position=2,mandatory=$false)]
	[String]$User,
	[Parameter(Position=3,mandatory=$false)]
	[String]$Output
)

# Administrator check
If ( -NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator") ) {
	Write-Warning "You do not have Administrator rights to run this script"
	Break
}

function get-extract {
	Try {
		if ( ( -not ([String]::IsNullOrEmpty($NTDS) ) ) -and ( -not ([String]::IsNullOrEmpty($Hive) ) ) ) {
			$dckey  = Get-BootKey -SystemHivePath $Hive
			$raw    = Get-ADDBAccount -All -DBPath $NTDS -BootKey $dckey | Where-Object SamAccountType -Like "User" | Format-Custom -View HashcatNT | Out-String
			$hashes = ForEach ( $obj in $raw.Split("`r`n") ) { $obj }
			Write-Host -ForegroundColor green "[+] Active Directory Extraction."
		} else {
			Write-Host -ForegroundColor red "Please provide information on the paths to the NTDS and the Hive."
			Break
		}
	} Catch {
		$msg = $_.Exception.Message
		Write-Host -ForegroundColor red "`n$msg`n"
		Write-Host -ForegroundColor red "[!] Active Directory Extraction Error.`n"
		Break
	}
	return $hashes
}

function export-hashes ($hashes) {
	Try {
		if ( -not ([String]::IsNullOrEmpty($hashes) ) ) {
			$hashes   = $hashes | Where-Object { -not ([String]::IsNullOrEmpty($_)) }
			$Folder   = Split-Path $Output -Parent
			$FileName = Split-Path $Output -Leaf
			$hashes | Out-File -FilePath $Output -Encoding UTF8
			Write-Host -ForegroundColor green "[+] RAW NTDS Export ($FileName)."
			$FileName = $FileName.Split(".")[0] + ".hash"
			$Convert  = "$Folder\" + "$FileName"
			$hashes | ForEach-Object {$_.Split(':')[1] } | Sort-Object -Unique | Out-File -FilePath $Convert -Encoding UTF8
			Write-Host -ForegroundColor green "[+] NTLM Hashes Export ($FileName)."
		} else {
			Write-Host -ForegroundColor red "Please provide hashes."
			Break
		}
	} Catch {
		$msg = $_.Exception.Message
		Write-Host -ForegroundColor red "`n$msg`n"
		Write-Host -ForegroundColor red "[!] Export Error."
		Break
	}
}

function get-user {
	Try {
		if ( ( -not ([String]::IsNullOrEmpty($NTDS) ) ) -and ( -not ([String]::IsNullOrEmpty($Hive) ) ) ) {
			$dckey  = Get-BootKey -SystemHivePath $Hive
			if ( -not [String]::IsNullOrEmpty($User) ) {
				Get-ADDBAccount -SamAccountName $User -DBPath $NTDS -BootKey $dckey | Where-Object SamAccountType -Like "User"
			} else {
				Write-Host -ForegroundColor red "Please provide a user."
				Break
			}
		} else {
			Write-Host -ForegroundColor red "Please provide information on the paths to the NTDS and the Hive."
			Break
		}
	} Catch {
		$msg = $_.Exception.Message
		Write-Host -ForegroundColor red "`n$msg`n"
		Write-Host -ForegroundColor red "[!] Active Directory Extraction Error.`n"
		Break
	}
	return $hashes
}

#Clear-Host
Write-Host -ForegroundColor DarkYellow "
           _   _ _____ ____  ____
          | \ | |_   _|  _ \/ ___|  _____  __
          |  \| | | | | | | \___ \ / _ \ \/ /
          | |\  | | | | |_| |___) |  __/>  < 
          |_| \_| |_| |____/|____/ \___/_/\_\
  
    ETTIC (ROP Team) @KASH______ - NTDS Extractor`n"

# Check
if ( -not (Get-Module -ListAvailable -Name DSInternals) ) {
	Write-Host -ForegroundColor red "`nDSInternals module is needed. (Install-Module DSInternals -Force)"
	Break
}
if ( [String]::IsNullOrEmpty($Output) ) {
	$Output = ".\$(Get-Date -Format 'yyyy-MM-dd')_ntds.raw"
}

# Main
if ( [String]::IsNullOrEmpty($User) ) {
$hashes = get-extract
export-hashes ($hashes)
} else {
	get-user
}
Write-Host "`nFinished`n"
